package models

import (
	"time"
)

type Exclusive struct {
	Id						int				`orm:"column(id);pk"`
	IdStation				int				`orm:"column(id_station)"`
	Date					time.Time 		`orm:"column(date);type(date)"`
	Type					int				`orm:"column(type);null"`
	OutsideTime				int				`orm:"column(outsidetime);null"`
	GoInside				int				`orm:"column(goinside);null"`
	Zone1					int				`orm:"column(zone1);null"`
	Zone2					int				`orm:"column(zone2);null"`
	Zone3					int				`orm:"column(zone3);null"`
	Special					int				`orm:"column(special);null"`
	CashierTime				int				`orm:"column(cashiertime);null"`
	BeforeServiceTime		int				`orm:"column(beforeservicetime);null"`
	ServiceTime				int				`orm:"column(servicetime);null"`
	TotalShow				int				`orm:"column(totalshow);null"`
	Buy						int				`orm:"column(buy);null"`
	TotalTimeIn				int				`orm:"column(totaltimein);null"`
	TotalTime				int				`orm:"column(totaltime);null"`
	StartOutdoor			time.Time 		`orm:"column(startoutdoor);type(time);null"`
	StartEntry				time.Time 		`orm:"column(startentry);type(time);null"`
	TimeBegin				time.Time 		`orm:"column(time_begin);type(time);null"`
	TimeEnd					time.Time 		`orm:"column(time_end);type(time);null"`
	OperatorId				string			`orm:"column(operator_id);size(50);null"`
	InsertDate				time.Time 		`orm:"column(insertdate);type(timestamp);auto_now_add;null"`
	TabletId				string			`orm:"column(tabletid);size(300);null"`
	IdHistoryOperators		int				`orm:"column(id_history_operators);null"`
	Service2				int				`orm:"column(service2);null"`
	FamilyTotal				int				`orm:"column(family_total);null"`
	GroupMan				int				`orm:"column(group_man);null"`
	GroupWoman				int				`orm:"column(group_woman);null"`
	LeftStore				int				`orm:"column(left_store);default(0);null"`
	NotSave					int				`orm:"column(not_save);default(0);null"`
	Descript				int				`orm:"column(descript);null"`
	Color					int				`orm:"column(color);null"`
	SaleTime				time.Time 		`orm:"column(sale_time);type(time);null"`
	Approach				int				`orm:"column(approach);null"`
	PresenceOn				int				`orm:"column(presence_on);null"`
	PresenceOff				int				`orm:"column(presence_off);null"`
	Checks					int				`orm:"column(checks);null"`
	AreaType				int				`orm:"column(area_type);default(1);null"`
	SittingAndCashier		int				`orm:"column(sittingandcashier);null"`
	EmployeeColor			int				`orm:"column(employeecolor);null"`
	PeopleInQueue			int				`orm:"column(peopleinqueue);null"`
	SessionIn				float64			`orm:"column(session_in);null"`
	Disappeared				int				`orm:"column(disappeared);null"`
	Deleted					int				`orm:"column(deleted);null"`
	Transactions			string			`orm:"column(transactions);type(jsonb);null"`
	CashierType				string			`orm:"column(cashiertype);type(text);null"`
	Note					string			`orm:"column(note);size(100);null"`
	YesNo					int				`orm:"column(yesno);default(0);null"`
	InstantCheck			int				`orm:"column(instantcheck);default(0);null"`
	UpdateTime				time.Time 		`orm:"column(update_time);type(timestamp);default(now());auto_now"`
}
