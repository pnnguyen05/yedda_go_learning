package models

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"golang.org/x/crypto/bcrypt"
	"io"
	"io/ioutil"
	"os"
)

// Encrypt password by "golang.org/x/crypto/bcrypt"
func HashAndSalt(password string, cost int) ( hashedPassword string, err error) {
	// Use GenerateFromPassword to hash & salt password
	// MinCost is just an integer constant provided by the bcrypt package along with DefaultCost & MaxCost
	hash, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	// GenerateFromPassword returns a byte slice so we need to convert the bytes to a string and return it
	return string(hash), err
}
func CheckPassword(hashedPassword string, plainPassword string) bool {
	// hashedPassword is encrypted password got from Database
	// plainPassword is entered password of user
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(plainPassword))
	if err != nil {
		return false
	}
	return true
}
// Other method to encrypt password
func CreateHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}
func EncryptPassword(plainPassword string, key string) string {
	// Creating a new block cipher based on the hashed plainPassword
	block, _ := aes.NewCipher([]byte(CreateHash(key)))
	// Wrap in Galois Counter Mode (GCM) with a standard nonce length.
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}
	cipherPassword := gcm.Seal(nonce, nonce, []byte(plainPassword), nil)
	return string(cipherPassword)
}
func DecryptPassword(cipherPassword string, key string) string {
	block, err := aes.NewCipher([]byte(CreateHash(key)))
	if err != nil {
		panic(err.Error())
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonceSize := gcm.NonceSize()
	nonce, cipherText := []byte(cipherPassword)[:nonceSize], []byte(cipherPassword)[nonceSize:]
	plainPassword, err := gcm.Open(nil, nonce, cipherText, nil)
	if err != nil {
		panic(err.Error())
	}
	return string(plainPassword)
}
func EncryptFile(filename string, data []byte, key string) {
	f, _ := os.Create(filename)
	defer f.Close()
	f.Write([]byte(EncryptPassword(string(data), key)))
}
func DecryptFile(filename string, passphrase string) []byte {
	data, _ := ioutil.ReadFile(filename)
	return []byte(DecryptPassword(string(data), passphrase))
}
