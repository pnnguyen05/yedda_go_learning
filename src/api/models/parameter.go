package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"time"
)

type Parameter struct {
	Id						int				`orm:"column(id_parameter);pk"`
	ExType					int				`orm:"column(ex_type);null"`
	NameBrief				string			`orm:"column(name_brief);size(50);null"`
	IdTemplate				int				`orm:"column(id_template);null"`
	IdTemplategroup			int				`orm:"column(id_templategroup);null"`
	NameParameter			string			`orm:"column(name_parameter);size(100);null"`
	ShortParameter			string			`orm:"column(short_parameter);size(100);null"`
	StringIdParameter		string			`orm:"column(stringid_parameter);size(50);null"`
	IdParent				int				`orm:"column(id_parent);null"`
	IdParentLevel1			int				`orm:"column(id_parent_level1);null"`
	IdParentLevel2			int				`orm:"column(id_parent_level2);null"`
	IdParentLevel3			int				`orm:"column(id_parent_level3);null"`
	IdParentLevel4			int				`orm:"column(id_parent_level4);null"`
	HebrewParameter			string			`orm:"column(hebrew_parameter);size(250);null"`
	HebrewShort				string			`orm:"column(hebrew_short);size(100);null"`
	RussianParameter		string			`orm:"column(russian_parameter);size(250);null"`
	RussianShort			string			`orm:"column(russian_short);size(100);null"`
	TurkeyParameter			string			`orm:"column(turkey_parameter);size(250);null"`
	TurkeyShort				string			`orm:"column(turkey_short);size(100);null"`
	VietnameseParameter		string			`orm:"column(vietnamese_parameter);size(250);null"`
	VietnameseShort			string			`orm:"column(vietnamese_short);size(100);null"`
	VietnamParameter		string			`orm:"column(vietnam_parameter);size(250);null"`
	ThailandParameter		string			`orm:"column(thailand_parameter);size(250);null"`
	ThailandShort			string			`orm:"column(thailand_short);size(100);null"`
	Actived					int				`orm:"column(actived);key(MUL);default(0);null"`
	Show					string			`orm:"column(show);size(10);null"`
	DataType				int				`orm:"column(data_type);default(0);null"`
	Upbad					int				`orm:"column(upbad);default(0);null"`
	AlertType				int				`orm:"column(alert_type);null"`
	IdColor					int				`orm:"column(id_color);null"`
	Image					string			`orm:"column(image);size(20);null"`
	TabletActived			int				`orm:"column(tablet_actived);default(0);null"`
	ParamType				int				`orm:"column(param_type);null"`
	Value1					int				`orm:"column(value_1);null"`
	TableActived			int				`orm:"column(table_actived);default(1);null"`
	IsRange					int				`orm:"column(is_range);default(0);null"`
	AreaType				int				`orm:"column(area_type);default(1);null"`
	UpdateTime				time.Time 		`orm:"column(update_time);type(timestamp);key(MUL);default(CURRENT_TIMESTAMP);auto_now"`
	HasFormula				int				`orm:"column(has_formula);default(0);null"`
	HasData					int				`orm:"column(has_data);default(1);null"`
	RuleKey					string			`orm:"column(rule_key);size(50);null"`
}

//var parameters []Parameter

func (t *Parameter) TableName() string {
	return "parameter"
}

func init() {
	orm.RegisterModel(new(Parameter))
}

// GetParameterById retrieves Parameter by Id
// Returns error if Id doesn't exist
func GetParameterById(id int) (v Parameter, err error) {
	// New orm with default database
	o := orm.NewOrm()
	// Switch database
	o.Using("remote")
	// Assign parameters
	parameter := Parameter{Id: id}
	// Read Parameter where Id (id_parameter) = id
	err = o.Read(&parameter)
	// Check read database result
	return parameter, err
}
func GetAllParameter() (parameters []Parameter, err error) {
	// New orm with default database
	o := orm.NewOrm()
	// Switch database
	o.Using("sql")
	// Read all parameters
	qb, err := orm.NewQueryBuilder("mysql")
	// Construct query object
	qb.Select("*").
		From("parameter").
		Limit(10).Offset(0)
	// Export raw query string from QueryBuilder object
	sql := qb.String()

	// Execute the raw query string
	o.Raw(sql).QueryRows(&parameters)
	//p, err := o.Raw("SELECT * FROM parameter").Prepare()
	//r, err := p.Exec()
	// Check read database result
	return parameters, err
}

// InsertParameter inserts a new Parameter into database
// Return last inserted Id on success and error if failed
func InsertParameter(newParam *Parameter) (id int64, err error) {
	o := orm.NewOrm()
	// Switch database
	o.Using("sql")
	lastId, err := o.Insert(newParam)
	return lastId, err
}

// UpdateParameterById updates Parameter by Id
// Returns error if the record to be updated doesn't exist
func UpdateParameterById(id int, param *Parameter) (num int64, err error) {
	// New orm with default database
	o := orm.NewOrm()
	// Switch database
	o.Using("sql")
	parameter := Parameter{Id: id}
	// Read Parameter where Id (id_parameter) = id
	err = o.Read(&parameter)
	// Ascertain id exists in the database
	if err == orm.ErrNoRows {
		fmt.Println("No result found.")
	} else if err == orm.ErrMissPK {
		fmt.Println("No primary key found.")
	} else {
		param.Id = id
		num, err = o.Update(param)
		if err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return num, err
}

// DeleteParameterById deletes Parameter by Id
// Returns error if Id doesn't exist
func DeleteParameterById(id int) (rownum int64, err error) {
	// New orm with default database
	o := orm.NewOrm()
	// Switch database
	o.Using("sql")
	// Assign parameters
	parameter := Parameter{Id: id}
	// Read Parameter where Id (id_parameter) = id
	rowNum, err := o.Delete(&parameter)
	return rowNum, err
}