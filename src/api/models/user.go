package models

import (
	"github.com/astaxie/beego/orm"
)

type User struct {
	IdCustomer				int				`orm:"column(id_customer);pk"`
	NameCustomer			string			`orm:"column(name_customer);size(255);null"`
	//LastName				string			`orm:"column(last_name);size(255);null"`
	Username				string			`orm:"column(username);size(255);null"`
	Password				string			`orm:"column(password);size(255);null"`
	//Salt					string			`orm:"column(salt);size(255);null"`
	//Address					string			`orm:"column(address);size(255);null"`
	//Phone					string			`orm:"column(phone);size(255);null"`
	//Sex						int				`orm:"column(sex);default(0);null"`
	//EmailBackup				string			`orm:"column(email_backup);size(255);null"`
	//Email					string			`orm:"column(email);size(255);null"`
	//Birthday				time.Time 		`orm:"column(birthday);type(date);null"`
	//DateCreate				time.Time 		`orm:"column(date_create);type(timestamp);auto_now_add;null"`
	UserType				int				`orm:"column(utype);default(1);null"`
	//Note					string			`orm:"column(note);size(255);null"`
	//Active					int				`orm:"column(active);null"`
	//TimeZone				int				`orm:"column(timezone);default(3);null"`
	//StorePrefix				string			`orm:"column(storeprefix);size(255);null"`
	//LanguageId				int				`orm:"column(language_id);null"`
	//LanguageOthers			string			`orm:"column(language_others);size(255);null"`
	//CountryId				int				`orm:"column(country_id);default(106);null"`
	//ActivationCode			string			`orm:"column(activationcode);size(255);null"`
	//DateInvitation			time.Time 		`orm:"column(date_invitation);type(timestamp);null"`
	//IdSegment				int				`orm:"column(id_segment);null"`
	//IdSegmentSub			int				`orm:"column(id_segmentsub);null"`
	//Exclusive				int				`orm:"column(exclusive);default(0);null"`
	//PermissionPeople		int				`orm:"column(permission_people);default(0);null"`
	//Prefix					string			`orm:"column(prefix);size(255);null"`
	//Agent					int				`orm:"column(agent);null"`
	//ShowOnList				int				`orm:"column(showonlist);default(1);null"`
	//IdPosition				int				`orm:"column(id_position);null"`
	//Demo					int				`orm:"column(demo);default(0);null"`
	//IdSalesFunnel			int				`orm:"column(id_sales_funnel);default(1);null"`
	//IdPartner				int				`orm:"column(id_partner);null"`
	//ClientStatus			int				`orm:"column(client_status);null"`
	//IsFirstLogin			int				`orm:"column(is_first_login);default(1);null"`
	//Knownledge				string			`orm:"column(knownledge);size(255);null"`
	//OnePager				string			`orm:"column(onepager);size(255);null"`
	//Presentation			string			`orm:"column(presentation);size(255);null"`
	//Trailing				string			`orm:"column(trailing);size(255);null"`
	//DemoStory				string			`orm:"column(demostory);size(255);null"`
	//DateExprie				time.Time 		`orm:"column(date_exprie);type(date);null"`
	//IdUserCategory			int				`orm:"column(id_usercategory);null"`
	//AppShowKpi				int				`orm:"column(app_show_kpi);default(1);null"`
	//ActiveKpi				int				`orm:"column(active_kpi);default(1);null"`
	//ActiveSmily				int				`orm:"column(active_smily);default(1);null"`
	//ActiveBar				int				`orm:"column(active_bar);default(1);null"`
	//AppBarDefault			int				`orm:"column(app_bar_default);default(0);null"`
	//Setting1				int				`orm:"column(setting1);default(0);null"`
	//SalesCashier			int				`orm:"column(sales_cashier);default(0);null"`
	IsNewStruct				int				`orm:"column(is_newstruct);default(0);null"`
	//TemplateType			int				`orm:"column(template_type);default(0);null"`
	IsDepartment			int				`orm:"column(is_department);null"`
	//WatchingStart			int				`orm:"column(watching_start);default(9);null"`
	//WatchingEnd				int				`orm:"column(watching_end);default(22);null"`
	//NumStores				int				`orm:"column(num_stores);null"`
	//CRMLink					string			`orm:"column(crm_link);size(255);null"`
	//CanAngie				int				`orm:"column(can_angie);null"`
	//IsMute					int				`orm:"column(is_mute);null"`
	//CompanyRank				int				`orm:"column(company_rank);null"`
	//ContactRank				int				`orm:"column(contact_rank);null"`
	//IsHideStory				int				`orm:"column(is_hide_story);null"`
	//IsIntroducer			int				`orm:"column(is_introducer);null"`
	//PotenialIntroducer		int				`orm:"column(potenial_introducer);null"`
	//TypeIntroducer			int				`orm:"column(type_introducer);null"`
	//FormalIntroducer		int				`orm:"column(formal_introducer);null"`
	//MotivationIntroducer	int				`orm:"column(motivation_introducer);null"`
	//InvestIntroducer		string			`orm:"column(invest_introducer);size(255);null"`
	//ContactChannel			int				`orm:"column(contact_channel);null"`
	//ContactInterval			int				`orm:"column(contact_interval);null"`
	//Market					int				`orm:"column(market);null"`
	//IdIntroducer			int				`orm:"column(id_introducer);null"`
	//IdManager				int				`orm:"column(id_manager);null"`
	//IdCompany				int				`orm:"column(id_company);null"`
	//StoreImage				string			`orm:"column(store_image);size(1000);null"`
	//AreaCoordinate			string			`orm:"column(area_coordinate);size(1000);null"`
	//Avatar					string			`orm:"column(avatar);size(255);null"`
	//UpdateTime				time.Time 		`orm:"column(update_time);type(timestamp);auto_now;null"`
	//Deleted					int				`orm:"column(deleted);default(0);null"`
	//NumStation				int				`orm:"column(num_station);null"`
}
func (u *User) TableName() string {
	return "user"

}
type Total struct {
	Total int
}

func init() {
	orm.RegisterModel(new(User))
}

func GetAllUsers() (users []User, err error) {
	o := orm.NewOrm()
	o.Using("default")
	_, err = o.Raw("SELECT * FROM public.user LIMIT 10").QueryRows(&users)
	return users, err
}
func GetUserById(idCustomer int) (user User, err error) {
	o := orm.NewOrm()
	o.Using("default")
	err = o.Raw("SELECT * FROM public.user WHERE id_customer = ?", idCustomer).QueryRow(&user)
	return user, err
}
func GetNewUsers(limit int64) (users []User, err error) {
	o := orm.NewOrm()
	o.Using("default")
	_, err = o.Raw("SELECT * FROM public.user ORDER BY id_customer DESC LIMIT ?", limit).QueryRows(&users)
	return users, err
}
func GetTotalUsers() (total Total, err error) {
	o := orm.NewOrm()
	o.Using("default")
	err = o.Raw("SELECT COUNT(*) AS total FROM public.user").QueryRow(&total)
	return total, err
}
func InsertNewUser(user User)  (idCustomer int64, err error){
	o := orm.NewOrm()
	o.Using("default")
	idCustomer, err = o.Insert(&user)
	return idCustomer, err
}
func InsertNewUsers(users []User)  (insertRows int64, err error){
	o := orm.NewOrm()
	o.Using("default")
	insertRows, err = o.InsertMulti(100, &users)
	return insertRows, err
}
func UpdateUsers(users User)  (updateRows int64, err error){
	o := orm.NewOrm()
	o.Using("default")
	updateRows, err = o.Update(&users)
	return updateRows, err
}
func DeleteUsers(users User)  (deleteRows int64, err error){
	o := orm.NewOrm()
	o.Using("default")
	deleteRows, err = o.Delete(&users)
	return deleteRows, err
}