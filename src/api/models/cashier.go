package models

import (
	"time"
)

type Cashier struct {
	Id						int				`orm:"column(id);pk"`
	AreaType				int				`orm:"column(area_type);default(1);null"`
	IdStation				int				`orm:"column(id_station);null"`
	IdClientstation			int				`orm:"column(id_clientstation);null"`
	RoomType				int				`orm:"column(roomtype);null"`
	Shift					int				`orm:"column(shift);null"`
	Date					time.Time 		`orm:"column(date);type(date);null"`
	DateIn					time.Time 		`orm:"column(datein);type(date);null"`
	TimeIn					time.Time 		`orm:"column(timein);type(time);null"`
	DateOut					time.Time 		`orm:"column(dateout);type(date);null"`
	TimeOut					time.Time 		`orm:"column(timeout);type(time);null"`
	InWatchingHour			int				`orm:"column(in_watchinghour);null"`
	TotalTime				int				`orm:"column(total_time);null"`
	NumberOfClients			int				`orm:"column(numberofclients);null"`
	Cashier					string			`orm:"column(cashier);size(20);null"`
	Employee				string			`orm:"column(employee);size(20);null"`
	Child 					string			`orm:"column(child);type(jsonb);null"`
	ReceiptId				string			`orm:"column(receiptid);size(50);null"`
	Room					string			`orm:"column(room);size(10);null"`
	Deleted					int				`orm:"column(deleted);default(0);null"`
	TimeBegin				time.Time 		`orm:"column(time_begin);type(time);null"`
	TimeEnd					time.Time 		`orm:"column(time_end);type(time);null"`
	Amount					float64			`orm:"column(amount);null"`
	Promotion				float64			`orm:"column(promotion);null"`
	Items					int				`orm:"column(items);null"`
	Status1					int				`orm:"column(status_1);default(0);null"`
	Status2					string			`orm:"column(status_2);size(20);null"`
	StatusCheck				int				`orm:"column(status_check);default(0);null"`
	IdEmployee				int				`orm:"column(id_employee);null"`
	IdCashier				int				`orm:"column(id_cashier);null"`
	Ticket					int				`orm:"column(ticket);null"`
	UpdateTime				time.Time 		`orm:"column(update_time);type(timestamp);default(now());auto_now;null"`
	IsManual				bool			`orm:"column(is_manual);default(false);null"`
}
