package models

import (
	"log"
	"os"
)

func OpenFile(filename string, data []byte, err error) {
	file, err := os.Open("file.go") // For read access.
	if err != nil {
		log.Fatal(err)
	}
	_, err = file.Read(data)
}
