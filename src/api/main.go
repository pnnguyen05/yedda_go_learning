package main

import (
	_ "api/routers"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"strconv"
	"strings"
)

func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	// Register Postgres Database only Read
	orm.RegisterDriver("postgres", orm.DRPostgres)
	pgdbReadHost := beego.AppConfig.String("pgdbReadHost")
	pgdbReadPort, _ := beego.AppConfig.Int("pgdbReadPort")
	pgdbReadUsername := beego.AppConfig.String("pgdbReadUsername")
	pgdbReadPassword := beego.AppConfig.String("pgdbReadPassword")
	pgdbReadDatabaseName := beego.AppConfig.String("pgdbReadDatabaseName")
	pgdbTemplateRead := "user=%s password=%s host=%s port=%d dbname=%s sslmode=disable search_path=public"
	connReadDefault := fmt.Sprintf(pgdbTemplateRead, pgdbReadUsername, pgdbReadPassword, pgdbReadHost, pgdbReadPort, pgdbReadDatabaseName)
	orm.RegisterDataBase("default","postgres",connReadDefault)
	// Register Postgres Database only Write
	orm.RegisterDriver("postgres", orm.DRPostgres)
	pgdbWriteHost := beego.AppConfig.String("pgdbReadHost")
	pgdbWritePort, _ := beego.AppConfig.Int("pgdbReadPort")
	pgdbWriteUsername := beego.AppConfig.String("pgdbReadUsername")
	pgdbWritePassword := beego.AppConfig.String("pgdbReadPassword")
	pgdbWriteDatabaseName := beego.AppConfig.String("pgdbReadDatabaseName")
	pgdbTemplateWrite := "user=%s password=%s host=%s port=%d dbname=%s sslmode=disable"
	connWriteDefault := fmt.Sprintf(pgdbTemplateWrite, pgdbWriteUsername, pgdbWritePassword, pgdbWriteHost, pgdbWritePort, pgdbWriteDatabaseName)
	orm.RegisterDataBase("defaultWrite","postgres",connWriteDefault)
	// Register MySQL Database only Read
	orm.RegisterDriver("mysql", orm.DRMySQL)
	mysqldbReadHost := beego.AppConfig.String("mysqldbReadHost")
	mysqldbReadPort, _ := beego.AppConfig.Int("mysqldbReadPort")
	mysqldbReadUsername := beego.AppConfig.String("mysqldbReadUsername")
	mysqldbReadPassword := beego.AppConfig.String("mysqldbReadPassword")
	mysqldbReadDatabaseName := beego.AppConfig.String("mysqldbReadDatabaseName")
	parts := []string{mysqldbReadUsername, ":", mysqldbReadPassword,
		"@tcp(", mysqldbReadHost, ":",strconv.Itoa(mysqldbReadPort),")/", mysqldbReadDatabaseName}
	orm.RegisterDataBase("sql", "mysql", strings.Join(parts, ""))
	beego.Run()
}
