package controllers

import (
	"api/models"
	_ "database/sql"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
)
// ParameterController operates for Parameter
type ParameterController struct {
	beego.Controller
}
// Define request struct for json body requests
type Request struct {
	Id int
}
// Define response struct for json body response
type Response struct {
	Data 		*models.Parameter
	UpdateNum	int64
	Status 		int
	AllData		*[]models.Parameter
}

func init() {
	//var parameter models.Parameter
}

func (this *ParameterController) Prepare() {

}
// @Title Get
// @Description find parameter by Id
// @Param	id		path 	string	true		"id parameter"
// @Success 200 {object} models.Object
// @Failure 403 :objectId is empty
// @router / [get]
func (this *ParameterController) Get() {
	//idStr := this.Ctx.Input.Param(":Id")
	// Assign parameter value from requests to variables
	var idParameter int
	this.Ctx.Input.Bind(&idParameter, "id")
	response := Response{}
	//fmt.Println(idParameter)
	//idParameter, err := strconv.Atoi(idStr)
	// Die if idParameter is not a number
	//if err != nil {
	//	panic(err)
	//}
	//parameter, err := models.GetParameterById(idParameter)
	parameter,err := models.GetAllParameter()
	if err == nil {
		response.Status = 1
	} else {
		response.Status = 0
	}
	response.AllData = &parameter
	this.Data["json"] = response
	fmt.Println(parameter)
	this.ServeJSON()
}
// @Title Create
// @Description create parameter
// @Param	body		body 	models.Parameter	true		"The parameter content"
// @Success 200 {string} models.Parameter.Id
// @Failure 403 body is empty
// @router / [post]
func (this *ParameterController) Post() {
	var updateParams models.Parameter
	json.Unmarshal(this.Ctx.Input.RequestBody, &updateParams)
	parameter, err := models.InsertParameter(&updateParams)
	response := Response{}
	if err != nil {
		updateParams.Id = -1
		response.Status = 0
	} else {
		updateParams.Id = int(parameter)
		response.Status = 1
	}
	response.Data = &updateParams
	this.Data["json"] = response
	this.ServeJSON()
}
// Put ...
// @Title Put
// @Description update the Parameter
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.Parameter	true		"body for Parameter content"
// @Success 200 {object} models.Host
// @Failure 403 :id is not int
// @router / [put]
func (this *ParameterController) Put() {
	var updateParams models.Parameter
	json.Unmarshal(this.Ctx.Input.RequestBody, &updateParams)
	var idParameter int
	this.Ctx.Input.Bind(&idParameter, "id")
	response := Response{}
	num, err := models.UpdateParameterById(idParameter, &updateParams)
	if err != nil {
		response.Status = 0
	} else {
		response.Status = 1
	}
	response.UpdateNum = num
	this.Data["json"] = response
	this.ServeJSON()
}

func (this *ParameterController) Delete() {

}


func (this *ParameterController) Head() {

}

func (this *ParameterController) Patch() {

}

func (this *ParameterController) Options() {

}

func (this *ParameterController) Finish() {

}