package controllers

import (
	"api/models"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"github.com/astaxie/beego"
)

// HostController operations for Host
type HostController struct {
	beego.Controller
}

// URLMapping ...
func (c *HostController) URLMapping() {
	//c.Mapping("Post", c.Post)
	//c.Mapping("GetOne", c.GetOne)
	//c.Mapping("GetAll", c.GetAll)
	//c.Mapping("Put", c.Put)
	//c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Post
// @Description create Host
// @Param	body		body 	models.Host	true		"body for Host content"
// @Success 201 {int} models.Host
// @Failure 403 body is empty
// @router / [post]
//const (
//	host     = "local.yedda.tech"
//	port     = 5432
//	user     = "wigo"
//	password = "IKjd$sh%42"
//	db   = "wigo"
//
//	//	pgdbReadAdapter = PDO_PGSQL
//	//	pgdbReadPort = 5432
//	//	pgdbReadHost = local.yedda.tech
//	//	username = wigo
//	//	pgdbReadPassword = IKjd$sh%42
//	//	pgdbReadName = wigo
//)
func (c *HostController) Post() {
	var v models.Host
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {
		if _, err := models.AddHost(&v); err == nil {
			c.Ctx.Output.SetStatus(201)
			c.Data["json"] = v
		} else {
			c.Data["json"] = err.Error()
		}
	} else {
		c.Data["json"] = err.Error()
	}
	//host := beego.AppConfig.String("pgdbReadHost")
	//port, _ := beego.AppConfig.Int("pgdbReadPort")
	//user := beego.AppConfig.String("pgdbReadUsername")
	//password := beego.AppConfig.String("pgdbReadPassword")
	//db := beego.AppConfig.String("pgdbReadName")
	//fmt.Println(host)
	//fmt.Println(port)
	//fmt.Println(user)
	//fmt.Println(password)
	//fmt.Println(db)
	//psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
	//	"password=%s dbname=%s sslmode=disable",
	//	beego.AppConfig.String("pgdbReadHost"), beego.AppConfig.String("pgdbReadPort"),
	//	beego.AppConfig.String("pgdbReadUsername"), beego.AppConfig.String("pgdbReadPassword"),
	//	beego.AppConfig.String("pgdbReadName"))
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, db)
	template := "user=%s password=%s host=%s port=%d dbname=%s sslmode=disable"
	connRemote := fmt.Sprintf(template, user, password, host, port, db)
	//db, err := sql.Open("postgres", psqlInfo)
	db, err := sql.Open("postgres", psqlInfo)
	//rows, err := db.Query("SELECT * FROM public.user WHERE id_customer = 2052")
	//stmt, err := db.Prepare("SELECT * FROM public.user WHERE id_customer = 2052")
	//rows, err := stmt.Exec()
	//for rows.Next() {
	//	var id_customer int
	//	rows.Scan(&id_customer)
	//	//fmt.Println(beego.AppConfig.String("db"))
	//	fmt.Printf("%3v", rows)
	//}
	a := orm.RegisterDataBase("defaultRead","postgres", connRemote)
	fmt.Println(db)
	fmt.Println(a)
	fmt.Println(psqlInfo)
	c.Data["json"] = err
	c.ServeJSON()
}

// GetOne ...
// @Title Get One
// @Description get Host by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Host
// @Failure 403 :id is empty
// @router /:id [get]
func (c *HostController) GetOne() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	v, err := models.GetHostById(id)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = v
	}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description get Host
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.Host
// @Failure 403
// @router / [get]
func (c *HostController) GetAll() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllHost(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = l
	}
	c.Data["json"] = "test"
	c.ServeJSON()
}

// Put ...
// @Title Put
// @Description update the Host
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.Host	true		"body for Host content"
// @Success 200 {object} models.Host
// @Failure 403 :id is not int
// @router /:id [put]
func (c *HostController) Put() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	v := models.Host{Id: id}
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {
		if err := models.UpdateHostById(&v); err == nil {
			c.Data["json"] = "OK"
		} else {
			c.Data["json"] = err.Error()
		}
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}

// Delete ...
// @Title Delete
// @Description delete the Host
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *HostController) Delete() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	if err := models.DeleteHost(id); err == nil {
		c.Data["json"] = "OK"
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}
const (
	host     = "local.yedda.tech"
	port     = 5432
	user     = "wigo"
	password = "IKjd$sh%42"
	db   = "wigo"
)