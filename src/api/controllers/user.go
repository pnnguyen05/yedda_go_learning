package controllers

import (
	"api/models"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	"strconv"
)

type UserController struct {
	beego.Controller
}
// Define request struct for json body requests
type Responses struct {
	Data 			*models.User
	IdCustomer 		int64
	UpdateNumber	int64
	InsertNumber	int64
	DeleteNumber	int64
	Status 			int
	Error			string
	Total 			*models.Total
	AllUsers		*[]models.User
	AllStores		*[]models.StoreOfCustomer
}

// @Title GetAll
// @Description get all users
// @Param	id		path 	string	true		"id_customer"
// @Success 200 {user} models.User
// @Failure 403 :userId is empty
// @router / [get]
func (this *UserController) GetAll() {
	response := Responses{}
	user, err := models.GetAllUsers()
	totalUser, err := models.GetTotalUsers()
	fmt.Println(totalUser)
	if err == nil {
		response.Status = 1
	} else {
		response.Status = 0
		response.Error = err.Error()
	}
	response.AllUsers = &user
	response.Total = &totalUser
	this.Data["json"] = response
	this.ServeJSON()
}
// @Title Get
// @Description get user by id
// @Param	id		path 	string	true		"id_customer"
// @Success 200 {user} models.User
// @Failure 403 :userId is empty
// @router /:idCustomer [get]
func (this *UserController) Get() {
	//var idCustomer int
	id := this.Ctx.Input.Param(":idCustomer")
	idCustomer, err := strconv.Atoi(id)
	//this.Ctx.Input.Bind(&idCustomer, "id")
	response := Responses{}
	//user, err := models.GetUserWithStationsById(idCustomer)
	user, err := models.GetStationsByIdCustomer(idCustomer)
	if err == nil {
		response.Status = 1
	} else {
		response.Status = 0
		response.Error = err.Error()
	}
	//response.AllStores = &user
	this.Data["json"] = user
	this.ServeJSON()
}
// @Title Create
// @Description create user
// @Param	body		body 	models.User 	true		"The user content"
// @Success 200 {string} models.User.Id
// @Failure 403 body is empty
// @router / [post]
func (this *UserController) Post() {
	newUsers := []models.User{}
	//newUsers := models.User{}
	json.Unmarshal([]byte(this.Ctx.Input.RequestBody), &newUsers)
	//json.Unmarshal(this.Ctx.Input.RequestBody, &newUsers)
	//newUsers.Password = models.EncryptPassword(newUsers.Password,"yedda")
	insertRows, err := models.InsertNewUsers(newUsers)
	//idCustomer, err := models.InsertNewUser(newUsers)
	response := Responses{}
	if err == nil {
		response.Status = 1
	} else {
		response.Status = 0
		response.Error = err.Error()
	}
	users, err := models.GetNewUsers(insertRows)
	response.AllUsers = &users;
	response.InsertNumber = insertRows
	//response.IdCustomer = idCustomer
	this.Data["json"] = response
	this.ServeJSON()
}
// @Title Update
// @Description update the users
// @Param	userId		path 	string	true		"The userId you want to update"
// @Param	body		body 	models.User		true		"The body"
// @Success 200 {object} models.User
// @Failure 403 :userId is empty
// @router /:userId [put]
func (this *UserController) Put() {
	userId := this.Ctx.Input.Param(":userId")
	idCustomer, err := strconv.Atoi(userId)
	updateUser := models.User{IdCustomer: idCustomer}
	json.Unmarshal(this.Ctx.Input.RequestBody, &updateUser)
	// Update users where idCustomer
	updateRows, err := models.UpdateUsers(updateUser)
	response := Responses{}
	if err == nil {
		response.Status = 1
	} else {
		response.Status = 0
		response.Error = err.Error()
	}
	response.UpdateNumber = updateRows
	this.Data["json"] = response
	this.ServeJSON()
}
// @Title Delete
// @Description delete the users
// @Param	userId		path 	string	true		"The userId you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 objectId is empty
// @router /:userId [delete]
func (this *UserController) Delete() {
	userId := this.Ctx.Input.Param(":userId")
	idCustomer, err := strconv.Atoi(userId)
	updateUser := models.User{IdCustomer: idCustomer}
	// Delete users where idCustomer
	deleteRows, err := models.DeleteUsers(updateUser)
	response := Responses{}
	if err == nil {
		response.Status = 1
	} else {
		response.Status = 0
		response.Error = err.Error()
	}
	response.DeleteNumber = deleteRows
	this.Data["json"] = response
	this.ServeJSON()
}