package main

import (
	"fmt"
	"ftp/models"
	_ "ftp/routers"
	"ftp/utils"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func main() {
	//var idCustomer string
	// Input ID customer
	//fmt.Println("Please input ID customer to send cashier report:")
	//fmt.Scanln(&idCustomer)
	idCustomer := os.Args[1]
	if idCustomer == "" {
		log.Fatal("Not found ID customer")
	}
	// Check existed log.txt
	currentDir, err := os.Getwd()
	logFolder := fmt.Sprintf("%s\\log", currentDir)
	logFile := fmt.Sprintf("%s\\%s.txt", logFolder, time.Now().Format("2006-01-02"))
	existed, err := models.FilesExist(logFile)
	if !existed && err == nil {
		os.MkdirAll(logFolder, os.ModePerm)
		_, _ = os.Create(logFile)
	}
	// Create model configuration for FTP connection and connect
	f := utils.CashierHost(idCustomer)
	sc, err := models.FTPConnection(&f)
	if err != nil {
		models.AppendFile(logFile,time.Now().String() + "\nCould not connect to FTP server. Please try again or check connection!")
		panic(err)
	} else {
		// Get recipient email by Id customer
		recipientEmail := utils.RecipientEmail(idCustomer)
		listFile, err := models.FTPGetListFiles(sc, f.BasePath)
		if len(listFile) > 0 {
			m := utils.SenderEmail(idCustomer, recipientEmail)
			listSentFiles, listFailRenamedFiles, listFailSentFiles, listTolargeFiles := "", "", "", ""
			numberSentFiles, numberFailRenamedFiles, numberFailSentFiles, numberToolargeFiles := 0, 0, 0, 0
			for i := range listFile {
				// Get filename and parent directory
				dir, file := filepath.Split(listFile[i])
				if strings.Contains(file, utils.CheckDone){
					continue
				}
				fileSize, err := models.FTPGetFileSize(sc, listFile[i])
				if fileSize >= utils.FileSize && err == nil {
					numberToolargeFiles++
					listTolargeFiles = listTolargeFiles + "\n" + file
					continue
				}
				rp, err := models.FTPGetFiles(sc, listFile[i])
				if err == nil {
					err = models.SendEmailReader(&m, file, rp)
					// Close file before implement new actions
					rp.Close()
					// Calculate number of files sent successfully
					// List files sent successfully
					if err == nil {
						numberSentFiles++
						listSentFiles = listSentFiles + "\n" + file
						// Change filename
						err = models.FTPRenameFile(sc,listFile[i], dir + utils.CheckDone + file)
						if err != nil {
							numberFailRenamedFiles++
							listFailRenamedFiles = listFailRenamedFiles + "\n" + file
						}
					} else {
						numberFailSentFiles++
						listFailSentFiles = listFailSentFiles + "\n" + file
					}
				}
			}
			// Write to log file
			models.AppendFile(logFile,"\n" + time.Now().String() + "\n" + fmt.Sprintf("[Number cashier reports were sent:] %d", numberSentFiles))
			models.AppendFile(logFile,"\n" + fmt.Sprintf("[List cashier reports were sent:]%s", listSentFiles))
			models.AppendFile(logFile,"\n" + fmt.Sprintf("[Number cashier reports were not sent:] %d", numberFailSentFiles))
			models.AppendFile(logFile,"\n" + fmt.Sprintf("[List cashier reports were not sent:]%s", listFailSentFiles))
			models.AppendFile(logFile,"\n" + fmt.Sprintf("[Number cashier reports were sent but not renamed:] %d", numberFailRenamedFiles))
			models.AppendFile(logFile,"\n" + fmt.Sprintf("[List cashier reports were sent but not renamed:]%s", listFailRenamedFiles))
			models.AppendFile(logFile,"\n" + fmt.Sprintf("[Number cashier report files are too large:] %d", numberToolargeFiles))
			models.AppendFile(logFile,"\n" + fmt.Sprintf("[List cashier report files are too large:]%s", listTolargeFiles))
		} else if err != nil {
			models.AppendFile(logFile,time.Now().String() + "\nCould not list files in folder. Please try again or check connection!")
		} else {
			models.AppendFile(logFile,time.Now().String() + "\nThe FTP folder is empty. Please check FTP data!")
		}
	}
	models.FTPQuit(sc)
	//beego.Run()
}

