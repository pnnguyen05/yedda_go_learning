package models

import (
	"github.com/gonutz/ftp-client/ftp"
	"io"
)

// Check FTP Client Connecttion
func FTPClientConnection(f *FTP) (*ftp.Connection, error){
	conn, err := ftp.Connect(f.Server, f.Port)
	//client, err := ftp.Dial(f.Server)
	if err != nil {
		return nil, err
	}
	err = conn.Login(f.Username, f.Password)
	return conn, err
}
// Check FTP Client RenameFile
func FTPClientRenameFile(c *ftp.Connection, filePath string, newPath string) (error){
	err := c.RenameFromTo(filePath, newPath)
	return  err
}
// Check FTP Client Quit
func FTPClientQuit(c *ftp.Connection){
	defer c.Quit()
}
// Check FTP Client Download
func FTPClientDownload(c *ftp.Connection, filePath string) (destPath io.Writer, err error){
	err = c.Download(filePath, destPath)
	return destPath, err
}
// Check FTP Client Delete
func FTPClientDelete(c *ftp.Connection, filePath string) error{
	err := c.Delete(filePath)
	return  err
}
// Check FTP Client ListFileNames
func FTPClientListFileNames(c *ftp.Connection, filePath string) ([]string, error){
	fileList, err := c.ListFileNamesIn(filePath)
	return  fileList, err
}
// Check FTP Client ListFile
func FTPClientListFiles(c *ftp.Connection, filePath string) (string, error){
	files, err := c.ListFilesIn(filePath)
	return  files, err
}