package models

import (
	"fmt"
	"github.com/domodwyer/mailyak"
	"io"
	"net/smtp"
)

const (

)

type Mail struct {
	MailHost			string
	Port				int
	SenderUsername		string
	SenderAddress		string
	SenderPassword		string
	CashierAddress		string
	CashierUser			string
	Subject				string
	Body				string
}

func SendEmailReader(m *Mail, fileName string, reader io.Reader)  (err error){
	mail := mailyak.New(fmt.Sprintf("%s:%d",m.MailHost, m.Port), smtp.PlainAuth("", m.SenderAddress, m.SenderPassword, m.MailHost))
	mail.To(m.CashierAddress)
	mail.From(m.SenderAddress)
	mail.Subject(m.Subject)
	mail.Plain().Set(m.Body)
	mail.Attach(fileName, reader)
	err = mail.Send()
	return err
}
/*func SendEmail(m *Mail)  (err error){
	// New Gomail
	g := gomail.NewMessage()
	// Set sender, recipient e-mail address and subject, body, and attached files of new email
	g.SetAddressHeader("From", m.SenderAddress, m.SenderUsername)
	g.SetAddressHeader("To", m.CashierAddress,m.CashierUser)
	//m.SetAddressHeader("Cc", "dan@example.com", "Dan")
	g.SetHeader("Subject", m.Subject)
	g.SetBody("text/plain", m.Body)
	g.Attach("/files/sepFunction.xlam")
	// Define new authentic sender
	d := gomail.NewDialer(m.MailHost, m.Port, m.SenderAddress, m.SenderPassword)
	err = d.DialAndSend(g)
	return err
}*/
