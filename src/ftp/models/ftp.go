package models

import (
	"fmt"
	"github.com/jlaffaye/ftp"
)

type FTP struct {
	Server			string
	Username		string
	Password		string
	Port			uint16
	BasePath		string
}
// Check FTP connection
func FTPConnection(f *FTP) (*ftp.ServerConn, error){
	client, err := ftp.Dial(fmt.Sprintf("%s:%d", f.Server, f.Port))
	err = client.Login(f.Username, f.Password)
	return client, err
}
func FTPQuit(sc *ftp.ServerConn){
	defer sc.Quit()
}
// List all file in path
func FTPGetListFiles(sc *ftp.ServerConn, filePath string) (list []string, err error) {
	list, err = sc.NameList(filePath)
	if err != nil {
		return nil, err
	}
	return list, err
}
// Fetch files from FTP directory
func FTPGetFiles(sc *ftp.ServerConn, filePath string) (*ftp.Response, error) {
	response, err := sc.Retr(filePath)
	return response, err
}
// Rename file
func FTPRenameFile(sc *ftp.ServerConn, filePath string, newPath string) (error) {
	err := sc.Rename(filePath, newPath)
	return err
}
func FTPGetFileSize(sc *ftp.ServerConn, filePath string) (size int64, err error) {
	fileSize, err := sc.FileSize(filePath)
	if err != nil {
		return -1, err
	}
	return fileSize, err
}
// Create FTP directory
/*func FTPCreateDirectory(sc *ftp.ServerConn, dir string) error {
	err := sc.MakeDir(dir)
	return err
}
func FTPRenameFile(sc *ftp.ServerConn, filePath string, newPath string) (error) {
	err := sc.Rename(filePath, newPath)
	return err
}
// Store files from FTP remote to local
func FTPGetFilesFrom(sc *ftp.ServerConn, filePath string, offset uint64) (*ftp.Response, error) {
	response, err := sc.RetrFrom(filePath, offset)
	return response, err
}
// Change FTP direction
func FTPChangeDirection(sc *ftp.ServerConn, filePath string) (error) {
	err := sc.ChangeDir(filePath)
	return err
}
// Fetch files from FTP directory
func FTPUploadFiles(sc *ftp.ServerConn, filePath string, r io.Reader) (error) {
	err := sc.Stor(filePath, r)
	return err
}*/
