https://github.com/golang/go/wiki/settinggopath
default GOPATH: %USERPROFILE%/go
default GOROOT: C:\Go\
- Rebuild after modifying: Controllers

- The first is the use of the & operator to get the address of our value
- where *X means pointer to value of type X
type Saiyan struct {
	Name string
	Power int
}
func (s *Saiyan) Super() {
	s.Power += 10000
}
In the above code, we say that the type *Saiyan is the receiver of the Super method. We call Super like so:
	goku := &Saiyan{"Goku", 9001}
	goku.Super()
	fmt.Println(goku.Power) // will print 19001

- The result of new(X) is the same as &X{}:	

type Person struct {
	Name string
}
func (p *Person) Introduce() {
	fmt.Printf("Hi, I'm %s\n", p.Name)
}
type Saiyan struct {
	*Person
	Power int
}
// and to use it:
goku := &Saiyan{
	Person: &Person{"Goku"},
	Power: 9001,
}
goku.Introduce()

- Go doesn�t support overloading, instead:

- scores := []int{1,2,3,4,5}
slice := scores[2:4]
slice[0] = 999
fmt.Println(scores)
The output is [1, 2, 999, 4, 5]

//if err == orm.ErrNoRows {
	//	fmt.Println("No result found.")
	//} else if err == orm.ErrMissPK {
	//	fmt.Println("No primary key found.")
	//} else {
	//	fmt.Println(parameter.Id, parameter.NameParameter)
	//}


If a parameter is marked as required, Beego will return an error if the parameter is not present in the http request:
// @Param   brand_id    query   int true       "brand id"
(the true option in the annotation above indicates that brand_id is a required parameter)

If a parameter has a default value and it does not exist in the http request, Beego will pass that default value to the method:
// @Param   brand_id    query   int false  5  "brand id"
(the 5 in the annotation above indicates that this is the default value for that parameter)

The location parameter in the annotation indicates where beego will search for that parameter in the request (i.e. query, header, body etc.)
// @Param   brand_id    path    int     true  "brand id"
// @Param   category    query   string  false "category" 
// @Param   token   header  string  false "auth token"
// @Param   task    body    {models.Task} false "the task object"
If a parameter name in the http request is different from the method parameter name, you can �redirect� the parameter using the => notation. This is useful, for example, When a header name is X-Token and the method parameter is named x_token:
// @Param   X-Token=>x_token    header  string  false "auth token"
